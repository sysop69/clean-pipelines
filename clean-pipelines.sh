#!/bin/bash

# URL de GitLab
read -p "Entrez l'URL de GitLab (ex: https://gitlab.example.com): " gitlab_url

# ID du projet
read -p "Entrez l'ID du projet: " project_id

# Token d'authentification
read -p "Entrez le token d'authentification: " token

pipeline_statuses=$(curl --silent -H "PRIVATE-TOKEN: $token" "$gitlab_url/api/v4/projects/$project_id/pipelines?per_page=100" | grep -o '"status":[^,]*' | awk -F: '{print $2}' | tr -d '"')

echo "Statuts disponibles et leur nombre respectif:"
for status in $(echo "$pipeline_statuses" | sort | uniq); do
  count=$(echo "$pipeline_statuses" | grep -c "$status")
  echo "- $status: $count"
done

# Choisir un statut à supprimer
read -p "Entrez le statut des pipelines à supprimer (all pour tous): " pipeline_status
if [ "$pipeline_status" != "all" ]; then
  pipelines=$(curl --silent -H "PRIVATE-TOKEN: $token" "$gitlab_url/api/v4/projects/$project_id/pipelines?status=$pipeline_status&per_page=100" | grep -o '"id":[^,]*' | awk -F: '{print $2}' | tr -d '"')
else
  pipelines=$(curl --silent -H "PRIVATE-TOKEN: $token" "$gitlab_url/api/v4/projects/$project_id/pipelines?per_page=100" | grep -o '"id":[^,]*' | awk -F: '{print $2}' | tr -d '"')
fi

# Nombre de pipelines à supprimer
read -p "Combien de pipelines souhaitez-vous supprimer (0 pour tous): " number_of_pipelines
if [ "$number_of_pipelines" == "0" ]; then
  number_of_pipelines=$(echo "$pipelines" | wc -l)
fi

counter=0
for pipeline in $(echo "$pipelines" | tail -n "$number_of_pipelines"); do
  curl --silent -H "PRIVATE-TOKEN: $token" -X DELETE "$gitlab_url/api/v4/projects/$project_id/pipelines/$pipeline" > /dev/null
  counter=$((counter + 1))
  echo "Pipeline $pipeline supprimé"
done
