#!/usr/bin/env python3.9

import requests
import json
import sys
from termcolor import colored

# liste des couleurs dans /usr/local/lib/python3.9/dist-packages/termcolor/termcolor.py
# necessite pip install termcolor

# URL de GitLab
GITLAB_URL = input("Entrez l'URL de GitLab (ex: https://gitlab.example.com): ")


# Token d'authentification
PRIVATE_TOKEN = input("Entrez le token d'authentification: ")


# ID du projet
PROJECT_ID = input("Entrez l'ID du projet: ")

url = f"{GITLAB_URL}/api/v4/projects/{PROJECT_ID}/pipelines"
headers = {
    "Private-Token": PRIVATE_TOKEN
}

response = requests.get(url, headers=headers)

if response.status_code != 200:
    print(f"Impossible de récupérer les pipelines : {response.content}")
    sys.exit(1)
pipelines = json.loads(response.content)

statuts = {}
for pipeline in pipelines:
    statut = pipeline["status"]
    if statut in statuts:
        statuts[statut] += 1
    else:
        statuts[statut] = 1

print("Statuts disponibles :")
for statut, nombre in statuts.items():
    if statut == "success":
        print(f"{colored(statut, 'light_green')} : {nombre} pipelines")
    elif statut == "failed":
        print(f"{colored(statut, 'light_red')} : {nombre} pipelines")
    elif statut == "canceled":
        print(f"{colored(statut, 'light_yellow')} : {nombre} pipelines")
    elif statut == "skipped":
        print(f"{colored(statut, 'yellow')} : {nombre} pipelines")
    else:
        print(f"{statut} : {nombre} pipelines")

# Choisir un statut à supprimer
type_pipeline = input("Entrez le statut des pipelines à supprimer (all pour tous): ")
if type_pipeline != "all":
    if type_pipeline not in statuts:
        print("Le type de pipeline entré n'est pas disponible")
        sys.exit(1)

# Nombre de pipelines à supprimer
nombre_a_conserver = input("Combien de pipelines souhaitez-vous supprimer (0 pour tous) : ")
try:
    nombre_a_conserver = int(nombre_a_conserver)
except ValueError:
    print("Le nombre entré n'est pas valide")
    sys.exit(1)

nb_supprimes = 0
for pipeline in reversed(pipelines):
    if type_pipeline != "all" and pipeline["status"] != type_pipeline:
        continue
    if nombre_a_conserver > 0 and nb_supprimes >= nombre_a_conserver:
        break
    pipeline_id = pipeline["id"]
    response = requests.delete(f"{url}/{pipeline_id}", headers=headers)
    if response.status_code != 204:
        print(f"Impossible de supprimer le pipeline {colored(pipeline_id, 'light_red')} : {response.content}")
        sys.exit(1)
    else:
        print(f"Pipeline {colored(pipeline_id, 'light_green')} supprimé avec succès")
        nb_supprimes += 1

print("Opération terminée.")
