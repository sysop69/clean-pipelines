# 🛠️ Clean pipelines
  
Le script bash (intéractif),<br>
et  le script python<br>
Developpé à titre privé et à usage privé & pro<br>
 permet de nettoyer vos pipelines<br>
 [Develop with GitLab ](https://docs.gitlab.com/ee/api/)

## 🛠️ Dependencies bash
Aucune

## 🛠️ Dependencies python   
```pip install termcolor```

mais aussi<br>
```
import requests
import json
import sys
```
  
## ➤ License
Distributed under the GNU GPL License. See [GNU GPL](https://www.gnu.org/licenses/gpl-3.0.html) for more information.<br><br>
**Juste penser à citer l'auteur et partager vos ameliorations.**

## 🧑🏻‍💻 Usage<br>
Facilement adaptable pour les commits, projects, groups, etc... à supprimer<br><br>
le fonctionnement (pas le code) est similaire pour les deux.
idem pour le script python<br>
### mais la sortie python est faite de couleurs<br><br>
```code
Ces script vont vous posez 5 questions:
     - l'url de votre Gitlab (hébergé auto-hébergé, ou non)
     - l'ID du projet, marche également avec le nom du projet mais avec les risques d'erreurs liés au(x) majuscule(s) ou autre, mieux vaut l'id
     - le token d'authentification => 2 possibilités
            - Acces token depuis votre compte utilisateur, cocher api et read api au minimum 
              (pas sur qu'il faille le reste, à vous de faire des essaies)
            - soit un deploy token depuis le projet (Settings => Repository => Deploy tokens en cochant les 3 scopes)
     
là le script affichera les status disponible et leur nombre respectif
     
     - ensuite vous selectionnez ce que vous souhaitez supprimer 
       selectionner all revient à prendre tous les status disponible et ressortie par le script
       sinon taper le nom du status
       (si vous n'avez aucun pipeline failed par exemple, cela ne vous en proposera pas, c'est toute la magie de l'api)

     ATTENTION, CELA RECUPERE UNIQUEMENT LES 100 PLUS RÉCENTS OU SI VOUS PRÉFÉRÉ LES 100 DERNIERS JOUES
     DONC SI VOUS EN AVEZ 1000 A NETTOYEZ, VA FALLOIR Y ALLER AVEC PRUDENCE
     
     - puis le nombre que vous souhaitez supprimer 10, 1, 100 voir 200 (oui cela fonctionnera même s'il y a des limites à tout) 
       si vous aviez selectionné que les pipelines success, cela supprimera les x pipelines concerné 

la sortie affichera une ligne par pipeline supprimé avec son numéro

```
